'use strict';

(function ($) {

  var m = {

    size: {},
    aspectRatio: 406 / 720,
    images: ['/images/intro_bg.jpg'],
    templates: {},
    slides: [],

    init: function () {
      m.initDom();
      m.body.addClass('showlogo');
      m.initCarousel();
      m.initOverview();
      setTimeout(function () {
        m.resize(true);
      }, 1000);
    },

    initDom: function () {
      m.html = $('html,body');
      m.body = $('body');
      m.intro = $('#intro-container');
      m.menu = $('#menu-container');
      m.carMain = $('#carousel-main');
      m.carousel = $('> .carousel-inner', m.carMain);
      m.overview = $('#overview-container');
      m.measure = $('#measure');
      m.offset = $('header.header').height();
    },

    initVideos: function (init) {
      $('.video-container', m.carousel).each(function () {
        var self = $(this),
            videos = self.children('video'),
            mep;
        videos.each(function () {
          mep = $(this).data('mediaelementplayer');
          if (mep) {
            mep.remove();
          }
        });
        self.width(m.size.w).height(m.size.h);
        videos.mediaelementplayer({
          features: ['progress'],
          videoWidth: '100%',
          videoHeight: '100%',
          enableAutosize: true,
          alwaysShowControls: true,
          iPadUseNativeControls: false,
          iPhoneUseNativeControls: false,
          AndroidUseNativeControls: false
        });
      });
      if (init) {
        m.initVideoControls();
      }
    },

    initVideoControls: function () {
      $('.item', m.carousel).each(function () {
        var nav = $('.video-nav', this),
            vidObj = $('video', this),
            video = vidObj.data('mediaelementplayer'),
            play = $('.play', nav);
        if (video) {
          vidObj.on('pause ended', function () {
            play.addClass('paused');
          }).on('play', function () {
            play.removeClass('paused');
          });
          play.click(function () {
            if (play.hasClass('paused')) {
              video.play();
            } else {
              video.pause();
            }
          });
          $('.rewind', nav).click(function () {
            video.play();
            video.setCurrentTime(0);
            video.play();
          });
        }
      });
    },

    initCarousel: function () {
      m.carMain.on('slide.bs.carousel', function (e) {
        $('video', e.target).each(function () {
          $(this).data('mediaelementplayer').pause();
        });
      }).on('slid.bs.carousel', function () {
        var active = $('.item.active', m.carousel);
        $('li', m.dropdown)
          .removeClass('active')
          .eq(active.index())
          .addClass('active');
        if (m.body.hasClass('playnow')) {
          m.body.removeClass('playnow');
          $('video', active).each(function () {
            var video = $(this).data('mediaelementplayer');
            video.play();
          });
        }
      });
      $('.item', m.carousel).each(function () {
        m.slides.push($(this).attr('data-id'));
        $('> .fill > .row, video', this).each(function () {
          $(this).swipe({
            swipeLeft: function() { m.carMain.carousel('next'); },
            swipeRight: function() { m.carMain.carousel('prev'); }
          });
        });
      });
    },

    initOverview: function () {
      $('.overview', m.overview).each(function () {
        var overview = $(this),
            id = overview.attr('data-id');
        $('a', overview).click(function () {
          m.body.addClass('playnow');
          m.carMain.carousel(m.slides.indexOf(id));
        });
      });
    },

    resize: function (init) {
      clearTimeout(m.body.data('tmout'));
      m.body.data('tmout', setTimeout(function () {
        m.body.addClass('loading');
        m.size.w = m.measure.width();
        m.size.h = m.size.w * m.aspectRatio;
        m.initVideos(init);
        m.resizeSlides();
        m.body.removeClass('loading');
      }, 500));
      if (init) {
        $(window).resize(function () {
          m.resize();
        });
      }
    },

    resizeSlides: function () {
      var items = $('.item', m.carousel).removeAttr('style'),
          active = items.filter('.active').removeClass('active').index();
      m.max = 0;
      items.each(function () {
        var self = $(this);
        self.addClass('active');
        m.max = Math.max(m.max, self.height());
        self.removeClass('active');
      });
      items.css({height:m.max}).eq(active).addClass('active');
    }

  };

  $(document).ready(function () {
    m.init();
  });

  return m;

})(jQuery);
